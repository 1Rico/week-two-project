BOOUST Week 2 Project.

Aim is to:
Be able to create a simple HTML file and markup,
Be able to give a simple styling (in line) to the HTML markup,
Understand the fundamentals of JavaScript, such as Data types, Variables, Control structures and Iterative statements, Data Structures.

This Simple HTML does the following:
Accepts a name of any animal from a user
Checks if the name given is in a list of animals,
Gives a response to the user, if the name is there or not